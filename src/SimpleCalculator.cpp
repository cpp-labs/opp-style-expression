#include "SimpleCalculator.hpp"
#include <cmath>

using std::cos;
using std::sin;
using std::exp;

namespace calculator {
    SimpleCalculator::SimpleCalculator(const double& a_, const double& b_)
            : a(a_)
            , b(b_)
    {}

    double SimpleCalculator::compute(const double& x) const {
        return (exp(a * x) * cos(b - x)) / sin(a + x);
    }

    void SimpleCalculator::step_compute(const double& x1, const double& x2, double * result) const {
        double step_weight = (x2 - x1) / 10;
        int steps = 10;

        for (int i = 0; i < steps; i++) {
            result[i] = compute(x1 + (step_weight * i));
        }
    }
}