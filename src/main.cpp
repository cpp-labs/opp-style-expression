#include <iostream>
#include "SimpleCalculator.cpp"

using std::cout;
using std::cin;
using std::endl;

int main() {
    calculator::SimpleCalculator calc(3.16, 7.11);
    printf("compute() = %f\n\n", calc.compute(-1.23));

    double results[10];
    calc.step_compute(-2, -1, results);

    for (int i = 0; i < 10; i++) {
        printf("%d.  step_compute() = %f\n", i + 1, results[i]);
    }
}
